# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 10:17:47 2020

@author: ayusa
"""

"""
Kendi modelimizi eğiteceğimiz kısım burası.
kullanıcı başlat dediği zaman buradan kamera açılacak. 
"""
from tkinter import *
from tkinter.ttk import *
from PIL import ImageTk, Image
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import time
import arayuz_2 as ui

video_name = 0

threshold = 298
foods = []
ID = 0

# RGB değil BGR
blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)
yellow = (0, 255, 255)
rose_dust = (111, 94, 158)
cyan = (255, 153, 51)
black = [0, 0, 0]

# kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE,(3,3))
fgbg = cv.bgsegm.createBackgroundSubtractorMOG()

foods_colors = []
areas = []
sinif1_test_sayisi = 0
sinif2_test_sayisi = 0

# video özellikleri
cap = cv.VideoCapture(video_name, cv.CAP_DSHOW)
# _, first = cap.read()
# height = np.size(first, 0)
# width = np.size(first, 1)

# width = int(cap.get(3))
# height = int(cap.get(4))

# original_image = np.ndarray(shape=(height, width, 3), dtype=float)
global original_image

egitim_baslat = True


def visualization():
    blues_c1 = []
    greens_c1 = []
    reds_c1 = []

    blues_c2 = []
    greens_c2 = []
    reds_c2 = []
    for f in foods[:sinif1_test_sayisi]:
        blues_c1.append(f.blue_mean)
        greens_c1.append(f.green_mean)
        reds_c1.append(f.red_mean)

    for f in foods[sinif1_test_sayisi:]:
        blues_c2.append(f.blue_mean)
        greens_c2.append(f.green_mean)
        reds_c2.append(f.red_mean)

    fig_bg = plt.figure()
    plt.scatter(blues_c1, greens_c1, c="blue", marker='+', label="Mavi-Yeşil Sınıf 1")
    plt.scatter(blues_c2, greens_c2, c="green", marker='x', label="Mavi-Yeşil Sınıf 2")
    plt.xlabel("Maviler")
    plt.ylabel("Yeşiller")
    plt.title("Maviler - Yeşiller")
    plt.legend(bbox_to_anchor=(0.75, 1.16), loc='upper left')
    plt.show()

    fig_br = plt.figure()
    plt.scatter(blues_c1, reds_c1, c="red", marker='v', label="Mavi-Kırmızı Sınıf 1")
    plt.scatter(blues_c2, reds_c2, c="blue", marker='^', label="Mavi-Kırmızı Sınıf 2")
    plt.xlabel("Maviler")
    plt.ylabel("Kırmızılar")
    plt.title("Maviler - Kırmızılar")
    plt.legend(bbox_to_anchor=(0.75, 1.16), loc='upper left')
    plt.show()

    fig_gr = plt.figure()
    plt.scatter(greens_c1, reds_c1, c="red", marker='<', label="Yeşil-Kırmızı Sınıf 1")
    plt.scatter(greens_c2, reds_c2, c="orange", marker='>', label="Yeşil-Kırmızı Sınıf 2")
    plt.xlabel("Yeşiller")
    plt.ylabel("Kırmızılar")
    plt.title("Yeşiller - Kırmızılar")
    plt.legend(bbox_to_anchor=(0.75, 1.16), loc='upper left')
    plt.show()


class food:
    def __init__(self, x, y, w, h, ID, image):
        self.apricot = []
        self.minx = x
        self.miny = y
        self.maxx = w
        self.maxy = h
        self.ID = ID
        self.center_x = (self.minx + self.maxx) // 2
        self.center_y = (self.miny + self.maxy) // 2
        self.apricot.append([self.minx, self.miny, self.maxx, self.maxy, self.center_x, self.center_y])
        self.size = (self.maxx - self.minx) * (self.maxy - self.miny)
        # 		print("Kayısının boyutu: ",self.size)
        self.blue_mean = 0
        self.green_mean = 0
        self.red_mean = 0

        self.extract_pixels(image)

    def extract_pixels(self, image):

        red_pixels = []
        green_pixels = []
        blue_pixels = []

        ori_img = original_image[self.miny:self.maxy, self.minx: self.maxx]

        for x in range(image.shape[0]):
            for y in range(image.shape[1]):
                if not image[x, y] == 0:  # apply varsa
                    # 				if not (thresh[x,y,0] == 0 and thresh[x,y,1] == 0 and thresh[x,y,2] == 0):	# threshold
                    # 					foods_colors.append(image[x,y])
                    red_pixels.append(ori_img[x, y, 2])
                    green_pixels.append(ori_img[x, y, 1])
                    blue_pixels.append(ori_img[x, y, 0])

        # 		print("Red: :",len(red_pixels))
        # 		print("Grenn:: ",len(green_pixels))
        # 		print("Blues: ", len(red_pixels))

        self.blue_mean = sum(blue_pixels) / len(blue_pixels)
        self.green_mean = sum(green_pixels) / len(green_pixels)
        self.red_mean = sum(red_pixels) / len(red_pixels)

        foods_colors.append([self.blue_mean, self.green_mean, self.red_mean])

    def add(self, x, y, w, h):
        center_x = (x + w) // 2
        center_y = (y + h) // 2
        self.apricot.append([x, y, w, h, center_x, center_y])

    def isNear(self, x, y, miny_p):  # center_x - center_y - minY

        n = (x, y)
        n = np.array(n)
        found = False
        # bir kayısının x (yatay) boyutu kadar kenarlara daha bakıyoruz.
        img_threshold = self.maxy - self.miny

        # bir obje geldikten sonra aynı hizadan başka obje gelebilir sonra
        if y < self.apricot[-1][5]:
            return False, -1

        for v in self.apricot:
            v = np.array(v)
            minx = v[0]
            maxx = v[2]
            center_x = v[4]
            center_y = v[5]
            if miny_p > center_y and x < maxx + img_threshold and x > minx - img_threshold:  # y-maxy < threshold --- (y > maxy) and
                found = True

        if found:
            return True, self.ID
        return False, -1


def main(ID):
    egitim(ID)


def egitimi_baslat_func(devam, sinif):
    global sinif1_test_sayisi
    global sinif2_test_sayisi
    global cap
    global ID
    global original_image

    _, original_image = cap.read()
    # print("Frame 3 5", original_image[3][5])
    print("sınıf: ",sinif, " devam: ", devam)
    if devam:
        original_image = model_egitimi(original_image, ID)

    else:
        if sinif == 1:
            print("1 Sınıf eğitimi bitti")
            sinif1_test_sayisi = len(foods)
            print("1 sinif için Tespit Edilen:", sinif1_test_sayisi)
        elif sinif == 2:
            print("2 Sınıf eğitimi bitti")
            sinif2_test_sayisi = len(foods) - sinif1_test_sayisi
            print("2 sinif için Tespit Edilen:", sinif2_test_sayisi)

        elif sinif == -99:
            print("kamera kapat")
            cap.release()
            return

    image = cv.cvtColor(original_image, cv.COLOR_BGR2RGBA)
    image = Image.fromarray(image)
    image = ImageTk.PhotoImage(image=image)

    panel = Label(image=image)
    panel.image = image

    return panel


# TEK FRAME ÜZERİNDEN GÖRÜNTÜ İŞLEME - NESNE TESPİTİ YAPAR
def model_egitimi(frame, ID):
    global original_image

    original_image = frame
    tresh = fgbg.apply(original_image)

    countour, _ = cv.findContours(tresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    for i in countour:
        if cv.contourArea(i) < 300:
            continue
        areas.append(cv.contourArea(i))

        x1, y1, w, h = cv.boundingRect(i)

        x2 = x1 + w
        y2 = y1 + h
        center_x = (x1 + x1 + w) // 2
        center_y = (y1 + y1 + h) // 2

        found = False

        foo_id = ID
        for f in reversed(foods):
            t, foo_id = f.isNear(center_x, center_y, y1)
            if t:
                f.add(x1, y1, x2, y2)
                found = True
                break

        if not found:
            ID += 1
            # 			print("NOT FOUND! Add this:",center_x,center_y)
            foo = food(x1, y1, x2, y2, ID, tresh[y1:y2, x1:x2])  # [y1:y2, x1:x2]
            foods.append(foo)
            foo_id = ID

        # 		print("ID: {} minxx: {} maxx: {} miny: {} maxy: {} Center X: {} Center Y: {} "
        # 		.format(foo_id, x1,x2, y1, y2,center_x, center_y))

        print("FOODS:")
        for k in foods:
            print(k.ID, k.center_x, k.center_y)
        print()

        # cv.drawContours(original_image, i, -1, (0, 255, 255), 3)
        cv.circle(original_image, (center_x, center_y), 1, red, 5)  # center
        cv.rectangle(original_image, (x1, y1), (x2, y2), yellow, 4)

        str_id = "ID: " + str(foo_id)
        cv.putText(original_image, str_id, (center_x - 15, center_y - 15),
                   cv.FONT_HERSHEY_SIMPLEX, 0.5, cyan, 2)

    return original_image


def deneme(devam):
    global egitim_baslat
    egitim_baslat = devam
    print("deneme", egitim_baslat)
    # print("Open ", cap.isOpened())

    print("egitim. ", egitim_baslat)

    print("Deneme bitti.")


def egitim(ID):
    start_time = time.time()

    frame_counter = 1

    while cap.isOpened() and egitim_baslat == True:
        ex, original_image = cap.read()
        frame_counter += 1
        # negative = 255 - frame
        if not ex:
            break

        if egitim_baslat == False:
            print("eğitim sonlandırıldı")
            return

        tresh = fgbg.apply(original_image)

        countour, _ = cv.findContours(tresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        for i in countour:
            if cv.contourArea(i) < 700:
                # 			print("Contour area:",cv.contourArea(i))
                continue
            areas.append(cv.contourArea(i))
            print("Frame Num: ", frame_counter)

            x1, y1, w, h = cv.boundingRect(i)

            x2 = x1 + w
            y2 = y1 + h
            center_x = (x1 + x1 + w) // 2
            center_y = (y1 + y1 + h) // 2

            found = False

            foo_id = ID

            for f in reversed(foods):
                t, foo_id = f.isNear(center_x, center_y, y1)
                if t:
                    f.add(x1, y1, x2, y2)
                    found = True
                    break

            if not found:
                ID += 1
                # 			print("NOT FOUND! Add this:",center_x,center_y)
                foo = food(x1, y1, x2, y2, ID, tresh[y1:y2, x1:x2])  # [y1:y2, x1:x2]
                foods.append(foo)
                foo_id = ID

            # 		print("ID: {} minxx: {} maxx: {} miny: {} maxy: {} Center X: {} Center Y: {} "
            # 		.format(foo_id, x1,x2, y1, y2,center_x, center_y))

            print("FOODS:")
            for k in foods:
                print(k.ID, k.center_x, k.center_y)
            print()

            # cv.drawContours(original_image, i, -1, (0, 255, 255), 3)
            cv.circle(original_image, (center_x, center_y), 1, red, 5)  # center
            cv.rectangle(original_image, (x1, y1), (x2, y2), yellow, 4)

            str_id = "ID: " + str(foo_id)
            cv.putText(original_image, str_id, (center_x - 15, center_y - 15),
                       cv.FONT_HERSHEY_SIMPLEX, 0.5, cyan, 2)

    print("Toplam Tespit Edilen:", len(foods))
    print("Toplam Frame Sayısı: ", frame_counter)
    print("Toplam Çalışma Zamanı:", time.time() - start_time)


# 	cv.destroyAllWindows()

# visulazition()
if __name__ == "__main__":
    main(ID)
