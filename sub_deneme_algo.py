# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 10:17:47 2020

@author: ayusa
"""

"""
Kendi modelimizi eğiteceğimiz kısım burası.
kullanıcı başlat dediği zaman buradan kamera açılacak. 
"""
from tkinter import *
from tkinter.ttk import *
from PIL import ImageTk, Image
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import time
import arayuz_2 as ui
#import sklearn.external.joblib as extjoblib
import joblib   # model kaydet

# kullanılan modeller
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

video_name = 0

threshold = 298
foods = []
temp_foods = []
tekrar_test_yapildi_mi = False
eskileri_sildik_mi = False
ID = 0

# RGB değil BGR
blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)
yellow = (0, 255, 255)
rose_dust = (111, 94, 158)
cyan = (255, 153, 51)
black = [0, 0, 0]

# kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE,(3,3))
fgbg = cv.bgsegm.createBackgroundSubtractorMOG()

foods_colors = []
areas = []
sinif1_test_sayisi = 0
sinif2_test_sayisi = 0

kullanilan_model = None

# video özellikleri
cap = cv.VideoCapture(video_name, cv.CAP_DSHOW)
# _, first = cap.read()
# height = np.size(first, 0)
# width = np.size(first, 1)

# width = int(cap.get(3))
# height = int(cap.get(4))

# original_image = np.ndarray(shape=(height, width, 3), dtype=float)
global original_image

egitim_baslat = True
foods_colors = [[39.407276736493934, 40.08335170893054, 47.699007717750824],
 [46.93644214162349, 49.505008635578584, 60.67288428324698],
 [25.49976830398517, 28.470574606116774, 36.088276181649675],
 [35.35001337971635, 38.906877174203906, 59.00642226384801],
 [64.46352413019079, 63.95286195286195, 67.92312008978675],
 [35.24497991967871, 37.766586345381526, 44.023132530120485],
 [53.468756756756754, 54.340756756756754, 65.0598918918919],
 [34.83919902912621, 34.84364886731392, 41.760113268608414],
 [31.613695313850943, 33.11689434549542, 44.895728860453055],
 [43.65679574791192, 44.86427486712225, 58.622817008352314],
 [36.61446667890582, 41.24288599228933, 61.97705158803011],
 [33.28698079133147, 39.10408799868659, 65.31144311278936],
 [29.17297781761147, 30.728209724400628, 35.18888639928299],
 [32.083696243875885, 32.30076211213936, 36.28946652150245],
 [42.00922909880565, 43.86400651465798, 61.83903365906623],
 [36.661395741092136, 36.91039426523297, 45.73518869913557],
 [35.923847349335176, 37.97150751165602, 49.99412882058366],
 [46.31111111111111, 46.73792270531401, 56.7268115942029],
 [29.484812150279776, 30.470423661071145, 36.68225419664269],
 [35.152745251105905, 35.865209471766846, 45.12724434035909],
 [31.091932059447984, 67.94373673036094, 96.36709129511678],
 [26.61760782570031, 64.49829553875797, 102.22824959241144],
 [29.676360924683074, 69.86293810589113, 110.02535421327367],
 [46.25783199334627, 73.08538952037705, 103.86470751316884],
 [30.699635922330096, 73.0950647249191, 106.27912621359224],
 [27.13091432225064, 58.99264705882353, 109.99808184143222],
 [29.243716337522443, 77.59201077199282, 112.56508078994614],
 [54.06813489332416, 85.63833448038541, 111.36338609772884],
 [27.252048296679604, 62.0642518326865, 102.80659767141009],
 [33.330933333333334, 65.77146666666667, 102.73413333333333],
 [21.47106170946723, 60.37773093139134, 106.72958988118053],
 [52.5638899359216, 83.04297022238974, 107.18808895589898],
 [23.987858525426713, 63.81664613760338, 107.08076720042231],
 [32.35836810384794, 73.76796476587853, 107.08599907278628],
 [37.89128787878788, 75.05530303030304, 102.17992424242425],
 [35.08120770432067, 64.77355543987507, 101.75845913586673],
 [31.008486562942007, 63.783435486405786, 107.56969982712558],
 [35.65523436224073, 82.4006206108117, 117.54973052425282],
 [25.468726307808947, 71.16641394996209, 105.77880970432146],
 [52.104213357238905, 85.79090094128193, 117.25437023756163]]




def visualization_2():
    global foods_colors
    sinif1_test_sayisi = 20
    foods_colors = np.array(foods_colors)
    blues_c1 = foods_colors[:20, 0]
    greens_c1 = foods_colors[:20, 1]
    reds_c1 = foods_colors[:20, 2]

    blues_c2 = foods_colors[20:, 0]
    greens_c2 = foods_colors[20:, 1]
    reds_c2 = foods_colors[20:, 2]

    f, axes = plt.subplots(nrows=2, ncols=2, figsize=(5, 5))

    axes[0][0].scatter(blues_c1, greens_c1, c="blue", marker='+', label="Mavi-Yeşil Sınıf 1")
    axes[0][0].scatter(blues_c2, greens_c2, c="green", marker='x', label="Mavi-Yeşil Sınıf 2")
    axes[0][0].set_xlabel('Maviler', labelpad=-2)
    axes[0][0].set_ylabel('Yeşiller')

    axes[0][1].scatter(blues_c1, reds_c1, c="red", marker='v', label="Mavi-Kırmızı Sınıf 1")
    axes[0][1].scatter(blues_c2, reds_c2, c="blue", marker='^', label="Mavi-Kırmızı Sınıf 2")
    axes[0][1].set_xlabel('Maviler', labelpad=-2)
    axes[0][1].set_ylabel('Kırmızılar', labelpad=-5, rotation=90)
    axes[0][1].yaxis.set_label_position("right")
    axes[0][1].yaxis.tick_right()

    axes[1][0].scatter(greens_c1, reds_c1, c="red", marker='<', label="Yeşil-Kırmızı Sınıf 1")
    axes[1][0].scatter(greens_c2, reds_c2, c="orange", marker='>', label="Yeşil-Kırmızı Sınıf 2")
    axes[1][0].set_xlabel('Yeşiller')
    axes[1][0].set_ylabel('Kırmızılar')

    return f


def visualization():
    foods_colors = np.array(foods_colors)
    blues_c1 = foods_colors[:20,0]
    greens_c1 = foods_colors[:20,1]
    reds_c1 = foods_colors[:20,2]

    blues_c2 = foods_colors[20:,0]
    greens_c2 = foods_colors[20:,1]
    reds_c2 = foods_colors[20:,2]

    """
    for f in foods[:sinif1_test_sayisi]:
        blues_c1.append(f.blue_mean)
        greens_c1.append(f.green_mean)
        reds_c1.append(f.red_mean)

    for f in foods[sinif1_test_sayisi:]:
        blues_c2.append(f.blue_mean)
        greens_c2.append(f.green_mean)
        reds_c2.append(f.red_mean)
    """
    fig_bg = plt.figure()
    plt.scatter(blues_c1, greens_c1, c="blue", marker='+', label="Mavi-Yeşil Sınıf 1")
    plt.scatter(blues_c2, greens_c2, c="green", marker='x', label="Mavi-Yeşil Sınıf 2")
    plt.xlabel("Maviler")
    plt.ylabel("Yeşiller")
    plt.title("Maviler - Yeşiller")
    plt.legend(bbox_to_anchor=(0.75, 1.16), loc='upper left')
    plt.show()


    fig_br = plt.figure()
    plt.scatter(blues_c1, reds_c1, c="red", marker='v', label="Mavi-Kırmızı Sınıf 1")
    plt.scatter(blues_c2, reds_c2, c="blue", marker='^', label="Mavi-Kırmızı Sınıf 2")
    plt.xlabel("Maviler")
    plt.ylabel("Kırmızılar")
    plt.title("Maviler - Kırmızılar")
    plt.legend(bbox_to_anchor=(0.75, 1.16), loc='upper left')
    plt.show()

    fig_gr = plt.figure()
    plt.scatter(greens_c1, reds_c1, c="red", marker='<', label="Yeşil-Kırmızı Sınıf 1")
    plt.scatter(greens_c2, reds_c2, c="orange", marker='>', label="Yeşil-Kırmızı Sınıf 2")
    plt.xlabel("Yeşiller")
    plt.ylabel("Kırmızılar")
    plt.title("Yeşiller - Kırmızılar")
    plt.legend(bbox_to_anchor=(0.75, 1.16), loc='upper left')
    plt.show()


class food:
    def __init__(self, x, y, w, h, ID, image):
        self.apricot = []
        self.minx = x
        self.miny = y
        self.maxx = w
        self.maxy = h
        self.ID = ID
        self.center_x = (self.minx + self.maxx) // 2
        self.center_y = (self.miny + self.maxy) // 2
        self.apricot.append([self.minx, self.miny, self.maxx, self.maxy, self.center_x, self.center_y])
        self.size = (self.maxx - self.minx) * (self.maxy - self.miny)
        # 		print("Kayısının boyutu: ",self.size)
        self.blue_mean = 0
        self.green_mean = 0
        self.red_mean = 0

        self.extract_pixels(image)

    def extract_pixels(self, image):
        global foods_colors
        red_pixels = []
        green_pixels = []
        blue_pixels = []

        ori_img = original_image[self.miny:self.maxy, self.minx: self.maxx]

        for x in range(image.shape[0]):
            for y in range(image.shape[1]):
                if not image[x, y] == 0:  # apply varsa
                    # 				if not (thresh[x,y,0] == 0 and thresh[x,y,1] == 0 and thresh[x,y,2] == 0):	# threshold
                    # 					foods_colors.append(image[x,y])
                    red_pixels.append(ori_img[x, y, 2])
                    green_pixels.append(ori_img[x, y, 1])
                    blue_pixels.append(ori_img[x, y, 0])

        # 		print("Red: :",len(red_pixels))
        # 		print("Grenn:: ",len(green_pixels))
        # 		print("Blues: ", len(red_pixels))

        self.blue_mean = sum(blue_pixels) / len(blue_pixels)
        self.green_mean = sum(green_pixels) / len(green_pixels)
        self.red_mean = sum(red_pixels) / len(red_pixels)

        foods_colors = np.append(foods_colors, [self.blue_mean, self.green_mean, self.red_mean])
        #foods_colors.append([self.blue_mean, self.green_mean, self.red_mean])

    def add(self, x, y, w, h):
        center_x = (x + w) // 2
        center_y = (y + h) // 2
        self.apricot.append([x, y, w, h, center_x, center_y])

    def isNear(self, x, y, miny_p):  # center_x - center_y - minY

        n = (x, y)
        n = np.array(n)
        found = False
        # bir kayısının x (yatay) boyutu kadar kenarlara daha bakıyoruz.
        img_threshold = self.maxy - self.miny

        # bir obje geldikten sonra aynı hizadan başka obje gelebilir sonra
        if y < self.apricot[-1][5]:
            return False, -1

        for v in self.apricot:
            v = np.array(v)
            minx = v[0]
            maxx = v[2]
            center_x = v[4]
            center_y = v[5]
            if miny_p > center_y and x < maxx + img_threshold and x > minx - img_threshold:  # y-maxy < threshold --- (y > maxy) and
                found = True

        if found:
            return True, self.ID
        return False, -1


def main(ID):
    egitim(ID)

# kaydet butonuna basınca modeli dosyaya kayıt et.
def modeli_dosyaya_kaydet(tur, model_ismi):
    isim = "Kayıtlı Modeller/" + tur + "_" + model_ismi
    joblib.dump(kullanilan_model, isim+ ".pkl")

    # modelin özelliklerini txt dosyasına yaz.
    f = open(isim+"txt", "a")
    f.write("Test sayısı ", sinif1_test_sayisi+sinif2_test_sayisi)
    f.write("Doğruluk Yüzdesi: ")
    f.write("Algoritma: ", str(kullanilan_model).replace("()", ""))
    f.close()

# modeli seç dediği zaman dosyadan seçilen modeli getir. kullanilan_model e kaydet.
def modeli_dosyadan_getir(tur, model_ismi):
    isim = "Kayıtlı Modeller/" + tur + "_" + model_ismi + ".pkl"
    kullanilan_model = joblib.load(isim)

    r = open(isim + "txt", "r")
    info = r.read()     # 'Test sayısı: 40\nDoğruluk Yüzdesi: 95\nAlgoritma: KNN'
    r.close()
    return info


# algoritma ismine göre objesini oluştur.
def algoritma_egit(algo):
    algoritmalar = ["K En Yakın Komşu", "Liner Regresyon", "Destek Vektör Makineleri", "Gaussian Naive Bayes", "Karar Ağacı"]
    index = algoritmalar.index(algo)
    if index == 0:
        kullanilan_model = KNeighborsClassifier(n_neighbors = 5)
    elif index == 1:
        kullanilan_model = LogisticRegression(solver="liblinear")
    elif index == 2:
        kullanilan_model = SVC(kernel="linear")
    elif index == 3:
        kullanilan_model = GaussianNB()
    elif index == 4:
        kullanilan_model = DecisionTreeClassifier()

    x_table = []
    global eskileri_sildik_mi, temp_foods, foods

    # foods içinde değerler kesin var.
    for f in foods:
        a = [f.blue_mean, f.green_mean, f.red_mean]
        x_table.append(a)

    # eğer tekrar eğitim yaptıysak temp_foods içinde de değerler olacak.
    if eskileri_sildik_mi:
        for f in temp_foods:
            a = [f.blue_mean, f.green_mean, f.red_mean]
            x_table.append(a)
    y1 = np.zeros(sinif1_test_sayisi, dtype=int)
    y2 = np.ones (sinif2_test_sayisi, dtype=int)
    y = np.concatenate((y1, y2))

    # train test split ?????

    kullanilan_model.fit(x_table, y)



# global class_counter
class_counter = 1

def eskileri_sil(sinif, foods):
    #sinif /= 100
    global eskileri_sildik_mi, temp_foods
    eskileri_sildik_mi = True

    # ilk sınıfı tekrar kaydet
    # ikinciyi yedekle. komple sil.
    if sinif == 1:
        print(f"birinciyi tekrar kaydet.", foods[0].red_mean)
        temp_foods = foods[sinif1_test_sayisi:]
        del foods

    # ikinci sinifi tekrar kaydet
    # birinciyi yedekle. komple sil.
    elif sinif == 2:
        print(f"ikinciyi tekrar kaydet. {len(foods)}")
        temp_foods = foods[:sinif1_test_sayisi]
        del foods


def egitimi_baslat_func(devam, sinif):
    #gc.enable()
    global sinif1_test_sayisi, class_counter
    global sinif2_test_sayisi, eskileri_sildik_mi
    global cap
    global original_image
    test_sayisi = 0

    _, original_image = cap.read()


    if (sinif == 100 or sinif == 200) and eskileri_sildik_mi == False:
        sinif /= 100
        eskileri_sil(sinif, foods)

    if devam:
        original_image = model_egitimi(original_image)
    else:
        #print("class_counter",class_counter)
        if class_counter == 1 and sinif == 1:
            sinif1_test_sayisi = len(foods)
            test_sayisi = sinif1_test_sayisi
            #print("1 sinif için Tespit Edilen:", sinif1_test_sayisi)

            class_counter += 1
            #print(f"sınıfırıncı red. {foods[0].red_mean}")
            eskileri_sildik_mi = False

        elif class_counter == 2 and sinif == 2:
            sinif2_test_sayisi = len(foods) - sinif1_test_sayisi
            #print("2 sinif için Tespit Edilen:", sinif2_test_sayisi)
            test_sayisi = sinif2_test_sayisi
            class_counter += 1
            eskileri_sildik_mi = False

    return original_image, test_sayisi





# TEK FRAME ÜZERİNDEN GÖRÜNTÜ İŞLEME - NESNE TESPİTİ YAPAR
def model_egitimi(frame):
    global ID

    tresh = fgbg.apply(frame)

    countour, _ = cv.findContours(tresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    for i in countour:
        if cv.contourArea(i) < 300:
            continue
        areas.append(cv.contourArea(i))

        x1, y1, w, h = cv.boundingRect(i)

        x2 = x1 + w
        y2 = y1 + h
        center_x = (x1 + x1 + w) // 2
        center_y = (y1 + y1 + h) // 2

        found = False

        foo_id = ID
        for f in reversed(foods):
            t, foo_id = f.isNear(center_x, center_y, y1)
            if t:
                f.add(x1, y1, x2, y2)
                found = True
                break

        if not found:
            ID += 1
            # 			print("NOT FOUND! Add this:",center_x,center_y)
            foo = food(x1, y1, x2, y2, ID, tresh[y1:y2, x1:x2])  # [y1:y2, x1:x2]
            foods.append(foo)
            foo_id = ID

        # 		print("ID: {} minxx: {} maxx: {} miny: {} maxy: {} Center X: {} Center Y: {} "
        # 		.format(foo_id, x1,x2, y1, y2,center_x, center_y))

        # print("FOODS:")
        # for k in foods:
        #     print(k.ID, k.center_x, k.center_y)
        # print()

        # cv.drawContours(original_image, i, -1, (0, 255, 255), 3)
        cv.circle(frame, (center_x, center_y), 1, red, 5)  # center
        cv.rectangle(frame, (x1, y1), (x2, y2), yellow, 4)

        str_id = "ID: " + str(foo_id)
        cv.putText(frame, str_id, (center_x - 15, center_y - 15),
                   cv.FONT_HERSHEY_SIMPLEX, 0.5, cyan, 2)

    return frame


def egitim(ID):
    start_time = time.time()

    frame_counter = 1

    while cap.isOpened() and egitim_baslat == True:
        ex, original_image = cap.read()
        frame_counter += 1
        # negative = 255 - frame
        if not ex:
            break

        if egitim_baslat == False:
            print("eğitim sonlandırıldı")
            return

        tresh = fgbg.apply(original_image)

        countour, _ = cv.findContours(tresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        for i in countour:
            if cv.contourArea(i) < 700:
                # 			print("Contour area:",cv.contourArea(i))
                continue
            areas.append(cv.contourArea(i))
            print("Frame Num: ", frame_counter)

            x1, y1, w, h = cv.boundingRect(i)

            x2 = x1 + w
            y2 = y1 + h
            center_x = (x1 + x1 + w) // 2
            center_y = (y1 + y1 + h) // 2

            found = False

            foo_id = ID

            for f in reversed(foods):
                t, foo_id = f.isNear(center_x, center_y, y1)
                if t:
                    f.add(x1, y1, x2, y2)
                    found = True
                    break

            if not found:
                ID += 1
                # 			print("NOT FOUND! Add this:",center_x,center_y)
                foo = food(x1, y1, x2, y2, ID, tresh[y1:y2, x1:x2])  # [y1:y2, x1:x2]
                foods.append(foo)
                foo_id = ID

            # 		print("ID: {} minxx: {} maxx: {} miny: {} maxy: {} Center X: {} Center Y: {} "
            # 		.format(foo_id, x1,x2, y1, y2,center_x, center_y))

            print("FOODS:")
            for k in foods:
                print(k.ID, k.center_x, k.center_y)
            print()

            # cv.drawContours(original_image, i, -1, (0, 255, 255), 3)
            cv.circle(original_image, (center_x, center_y), 1, red, 5)  # center
            cv.rectangle(original_image, (x1, y1), (x2, y2), yellow, 4)

            str_id = "ID: " + str(foo_id)
            cv.putText(original_image, str_id, (center_x - 15, center_y - 15),
                       cv.FONT_HERSHEY_SIMPLEX, 0.5, cyan, 2)

    print("Toplam Tespit Edilen:", len(foods))
    print("Toplam Frame Sayısı: ", frame_counter)
    print("Toplam Çalışma Zamanı:", time.time() - start_time)


# 	cv.destroyAllWindows()

# visulazition()
if __name__ == "__main__":
    main(ID)
