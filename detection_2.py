#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 10:36:28 2020

@author: yusa
"""


import cv2 as cv
import numpy as np

cap = cv.VideoCapture("kayisi.mp4")

# RGB değil BGR
yellow = (0, 255, 255)
rose_dust = (111, 94, 158)
green = (255, 0, 0)

img_counter = 1


while True:
    _, original_image = cap.read()
    # negative = 255 - frame
    # cv.imshow('Orginal Video',frame)
    
    original_grey = cv.cvtColor(original_image, cv.COLOR_BGR2GRAY)
    original_grey = cv.Canny(original_grey, 100, 200)
    
    _, tresh = cv.threshold(original_grey, 20,255, cv.THRESH_BINARY)
    dilation = np.zeros((5,5), np.uint8)
    tresh = cv.dilate(tresh, dilation)
    
    # CV_RETR_EXTERNAL gives "outer" contours, so if you have (say) 
    # one contour enclosing another (like concentric circles),
    # only the outermost is given.
    countour,_ = cv.findContours(tresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    
    print("Len cont:", len(countour))
    for i in countour:
        # if cv.contourArea(i) < 10:
        #     continue
        
        # print("Len cont:", len(countour))
        # x, y, w, h = cv.boundingRect(i)
        
        
        x1,y1,w,h = cv.boundingRect(i)

        x2 = x1 + w
        y2 = y1 + h
        
        # center_X = int(M['m10']/M['m00'])
        # center_y = int(M['m01']/M['m00'])
                
        print("Cekilen Image: X1: {} X2: {}, Y1: {}, Y2: {} ".format(x1, x2, y1, y2))
        
        # if x2 - x1 >= 200 and y2 - y1 >= 200:
        # saved = original_image[x1 : x2, y1 : y2]
        # img_name = "C:/Users/ayusa/Desktop/Bitirme/photos/" + str(img_counter) + "_img.jpg"
        # print(str(img_counter) + " saved ")
        # cv.imwrite(img_name, saved)
        # img_counter += 1
        cv.drawContours(original_image, i, -1, (0, 255, 255), 3) 
        # cv.rectangle(original_image, (x1, y1), (x2, y2), (0,255,255), 4)
        
    cv.imshow("Image Detection",original_image)
    
    key = cv.waitKey(0)
    while key not in [ord('q'), ord('k')]:
        key = cv.waitKey(0)
    # Quit when 'q' is pressed
    if key == ord('q'):
        break
    
    # ESC = EXIT
    # esc = cv.waitKey(5) & 0xFF
    # if esc == 27:
    #     break
    
    
    

cv.destroyAllWindows()