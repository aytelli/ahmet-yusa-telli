# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 22:05:01 2020

@author: ayusa
"""

from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *
from PIL import ImageTk, Image
import cv2
import numpy as np
import sub_deneme as algo
import warnings
import threading
import imutils
from time import sleep
warnings.filterwarnings("ignore")
video_name = 0
kayitli_turler = []
test_veri_sayisi = 0
egitime_devam_mi = False
sinif = -2

class Tur_Sinifi:
    def __init__(self, name):
        self.nesne_turu = name
        self.kayitli_modeller = []
        self.kayitli_modeller.append(name)

    def model_kaydet(self, yeni_model):
        self.kayitli_modeller.append(yeni_model)

    def getTurIsmi(self):
        return self.nesne_turu

    def getModeller(self):
        return self.kayitli_modeller


class Tum_Turler_ve_Modeller:
    def __init__(self):
        self.turler = []
        self.modeller = []

    def tur_ekle(self, tur):
        self.turler.append(tur)
        self.modeller.append([tur])

    def model_ekle(self, tur, model):
        lower_list = [item.lower() for item in self.turler]
        index = lower_list.index(tur.lower())
        #print("index: ", index)
        if index == -1:
            self.turler.append(tur)
            self.modeller.append([model])
        else:
            self.modeller[index].append(model)

    def tur_getir(self):
        return self.turler

    def model_getir(self):
        return self.modeller

    def model_getir_turegore(self, tur):
        lower_list = [item.lower() for item in self.turler]
        index = lower_list.index(tur.lower())
        return self.modeller[index]


class MainWindow:
    def __init__(self, root):
        #global tur
        global siniflar_liste
        global algoritmalar
        self.root = root
        self.root.geometry("1200x600")
        self.root.title("Falling Object Classification")

        self.cap = cv2.VideoCapture(0)

        # KAMERA
        self.panel = None
        self.frame = None
        self.stopEvent = threading.Event()
        self.thread = threading.Thread(target=self.kamera, args=())
        self.thread.start()

        # self.cerceve_kamera = Frame(root)
        # self.cerceve_kamera.grid(row=0, column=0, rowspan=15)  # row=0, column=0
        # self.kamera_label = Label(self.cerceve_kamera)
        # self.kamera_label.grid(row=0, column=0, rowspan=15)

        #
        # width = int(self.cap.get(3))
        # height = int(self.cap.get(4))
        # self.frame = np.ndarray(shape=(height, width, 3), dtype=float)
        # # function for video streaming
        # if self.cap.isOpened():
        #     self.video_stream()
        # else:
        #     print("kamera açılmadı")
        #     messagebox.showerror("Kamera Hata", "Kamera açılmadı veya bulunamadı.")



        # ANA EKRAN
        self.mevcut_modeller = Button(root, text="Mevcut Modeller", command=self.mevcut_model_penceresi)
        self.yeni_model_buton = Button(root, text="Yeni Model Eğit", command=self.yeni_model_penceresi)
        self.exit_button = Button(root, text="Çıkış", command=self.cikis)

        # MEVCUT MODELLER MENUSU
        # geri butonu
        # combo box
        # radio butonları
        # model seç butonu
        self.r_var = StringVar()
        self.modeller_rd = []
        self.mevcut_tur_secin_yazisi = Label(root, text="Eğitilecek Türü Seçiniz:")
        self.geri_buton = Button(root, text="Geri", command=self.geri_butonu_tiklandi)
        self.model_listesi_cb = Combobox(root)
        #self.model_listesi_cb['values'] = tur.tur_getir()
        self.model_listesi_cb['values'] = siniflar_liste
        #self.model_listesi_cb.current(0)
        self.model_listesi_cb.bind("<<ComboboxSelected>>", self.set_radio)
        self.model_sec_buton = Button(root, text="Modeli Seç", command=self.hangi_model_secildi)




        # YENİ MODEL EGİT MENUSU
        # geri butonu
        # İsim Girin yazısı
        # isim alma yeri ve giriş butonu
        # 1. sınıf için yazı, başlat - bitir butonları
        # 2. sınıf için yazı, başlat - bitir butonları
        # Algoritma Seçme Yazısı
        # Algoritmalar Radio butonları
        # başlat buton
        self.yeni_model_ismi_girin = Label(root, text="Yeni Modele İsim Girin:")
        self.yeni_model_ismi_input = Entry(root)
        self.yeni_model_ismi_giris = Button(root, text="Giriş", command=self.yeni_sinif_ekleme)
        self.sinif1_text = Label(root, text="İlk sınıf için Eğitim: ")
        self.sinif1_baslat = Button(root, text="Başlat", command=lambda: self.egitimi_baslat(True, 1))
        self.sinif1_bitir = Button(root, text='Bitir', command=lambda: self.egitimi_durdur(False, 1))
        self.sinif2_text = Label(root, text="İkinci sınıf için Eğitim: ")
        self.sinif2_baslat = Button(root, text="Başlat", command=lambda: self.egitimi_baslat(True, 2))
        self.sinif2_bitir = Button(root, text='Bitir', command=lambda: self.egitimi_durdur(False, 2))
        self.algoritma_seciniz = Label(root, text= "Algoritma Seçiniz:")
        self.algoritma_radios = []
        self.a_var = StringVar()
        self.yeni_model_baslat = Button(root, text= "Testi Başlat", command=self.algoritma_secildi)



        self.AnaEkran()

    def AnaEkran(self):
        self.herseyi_sil()
        self.mevcut_modeller.grid(row=1, column=1, ipady=15)
        self.yeni_model_buton.grid(row=1, column=2, ipady=15)
        self.exit_button.grid(row=9, column=1, sticky= W)

    def kamera(self):
        global egitime_devam_mi
        global sinif
        try:
            while not self.stopEvent.is_set():
                self.panel = algo.egitimi_baslat_func(egitime_devam_mi, sinif)
                self.panel.grid(row=0, column=0, rowspan=20)
        except RuntimeError as e:
            print("[INFO] caught a RuntimeError")

    def set_radio(self, event):
        for w in self.modeller_rd:
            w.destroy()
        secilen = self.model_listesi_cb.get()
        index = -1
        #radio_values = tur.model_getir_turegore(secilen)
        index = -1
        for i, t in enumerate(kayitli_turler):
            if secilen == t.getTurIsmi():
                index = i
        radio_values = kayitli_turler[index].getModeller()
        for num, t in enumerate(radio_values, 1):
            b = Radiobutton(root, text=t, variable=self.r_var, value=t)
            b.grid(row=num+2, column=1)
            self.modeller_rd.append(b)
            index = num

        self.model_sec_buton.grid(row=index+3, column=1)

    def mevcut_model_penceresi(self):
        self.herseyi_sil()
        self.geri_buton.grid(row=0, column=1, sticky=W)
        self.mevcut_tur_secin_yazisi.grid(row=1, column=1)
        self.model_listesi_cb.grid(row=2, column=1)

    def yeni_model_penceresi(self):
        self.herseyi_sil()
        self.geri_buton.grid(row=0, column=1)
        self.yeni_model_ismi_girin.grid(row=1, column=1, columnspan=2)
        self.yeni_model_ismi_input.grid(row=2, column=1)
        self.yeni_model_ismi_giris.grid(row=2, column=2)

    def yeni_sinif_ekleme(self):
        self.sinif1_text.grid(row=3, column=1, columnspan=2)
        self.sinif1_baslat.grid(row=4, column=1)
        self.sinif1_bitir.grid(row=4, column=2)
        self.sinif2_text.grid(row=5, column=1, columnspan=2)
        self.sinif2_baslat.grid(row=6, column=1)
        self.sinif2_bitir.grid(row=6, column=2)
        self.algoritma_seciniz.grid(row=7, column=1, columnspan=2)
        i = 8
        for algo in algoritmalar:
            rb = Radiobutton(root, text=algo, variable= self.a_var, value= algo)
            rb.grid(row=i, column= 1, columnspan=2)
            self.algoritma_radios.append(rb)
            i += 1

        self.yeni_model_baslat.grid(row=i+1, column=1, ipadx= 20, columnspan=2)

    def geri_butonu_tiklandi(self):
        self.AnaEkran()

    def video_stream(self):
        self.add += 1
        _, self.frame = self.cap.read()
        #print("Video Stream 3 5:", self.frame[3][5])
        cv2image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGBA)
        img = Image.fromarray(cv2image)
        imgtk = ImageTk.PhotoImage(image=img)
        self.kamera_label.imgtk = imgtk
        self.kamera_label.configure(image=imgtk)
        self.kamera_label.after(1, self.video_stream)

    def videoLoop(self):
        global egitime_devam_mi
        global sinif
        try:
            while not self.stopEvent.is_set():
                self.frame = algo.egitimi_baslat_func(egitime_devam_mi, sinif)
                #print("Gelen f", self.frame[3][5])
                image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGBA)
                image = Image.fromarray(image)
                image = ImageTk.PhotoImage(image=image)

                if self.panel is None:
                    self.panel = Label(image=image)
                    self.panel.image = image
                    self.panel.grid(row=0, column=0, rowspan=20)

                else:
                    self.panel.configure(image=image)
                    self.panel.image = image
        except RuntimeError as e:
            print("[INFO] caught a RuntimeError")




    def herseyi_sil(self):
        # ana ekranı sil
        self.mevcut_modeller.grid_remove()
        self.yeni_model_buton.grid_remove()
        self.exit_button.grid_remove()
        # mevcut model ekranı sil
        self.geri_buton.grid_remove()
        self.mevcut_tur_secin_yazisi.grid_remove()
        self.model_sec_buton.grid_remove()
        self.model_listesi_cb.grid_remove()
        for rad in self.modeller_rd:
            rad.destroy()
        # yeni model ekranı sil
        self.yeni_model_ismi_girin.grid_remove()
        self.yeni_model_ismi_input.grid_remove()
        self.yeni_model_ismi_giris.grid_remove()
        self.sinif1_text.grid_remove()
        self.sinif1_baslat.grid_remove()
        self.sinif1_bitir.grid_remove()
        self.sinif2_text.grid_remove()
        self.sinif2_baslat.grid_remove()
        self.sinif2_bitir.grid_remove()
        self.algoritma_seciniz.grid_remove()
        for a in self.algoritma_radios:
            a.destroy()
        self.yeni_model_baslat.grid_remove()


    def egitimi_baslat(self, devam, sin):
        global egitime_devam_mi
        global sinif
        sinif = sin
        egitime_devam_mi = devam
        #self.egitime_gonder(sinif)

    def egitimi_durdur(self, devam, sin):
        global egitime_devam_mi
        global sinif
        sinif = sin
        egitime_devam_mi = devam
        sleep(1)
        sinif = -1


    def egitime_gonder(self, sinif):
        global egitime_devam_mi
        self.frame = algo.egitimi_baslat_func(egitime_devam_mi, sinif)
        #if egitime_devam_mi:
        #    root.after(10, lambda:self.egitime_gonder(sinif))

    def algoritma_secildi(self):
        print("Seçilen algoritma:", self.a_var.get())
        algo.visualization()

    def hangi_model_secildi(self):
        print("Seçilen tür:", self.model_listesi_cb.get())
        print("Seçilen model: ", self.r_var.get())

    def cikis(self):
        global egitime_devam_mi
        global sinif
        egitime_devam_mi = False
        sinif = -99
        self.herseyi_sil()
        self.stopEvent.set()
        self.root.quit()


if __name__ == '__main__':
    kayisi = Tur_Sinifi('Kayısı')
    elma = Tur_Sinifi('Elma')
    incir = Tur_Sinifi('İncir')

    kayitli_turler.append(kayisi)
    kayitli_turler.append(elma)
    kayitli_turler.append(incir)

    kayisi.model_kaydet('model 1')
    kayisi.model_kaydet('model 2')
    kayisi.model_kaydet('model 3')
    elma.model_kaydet('elma 1')
    elma.model_kaydet('elma 2')
    incir.model_kaydet('incir 1')
    siniflar_liste = []
    print("Elma modelleri:", elma.getModeller())
    for n in kayitli_turler:
        siniflar_liste.append(n.getTurIsmi())

    algoritmalar = ["KNN", "CNN", "Yapay Sinir Ağı", "İkili Sınıflandırma Algoritması"]

    # tur = Tum_Turler_ve_Modeller()
    # tur.tur_ekle("Kayısı")
    # tur.tur_ekle("Elma")
    # tur.tur_ekle("Armut")
    #
    # tur.model_ekle("kayısı", "kayisi 1")
    # tur.model_ekle("kayısı", "kayisi 2")
    # tur.model_ekle("kayısı", "kayisi 3")
    # tur.model_ekle("elma", "elma 1")
    # tur.model_ekle("elma", "elma 2")
    # tur.model_ekle("elma", "elma 3")
    # tur.model_ekle("elma", "elma 4")
    # tur.model_ekle("armut", "armut 1")
    # tur.model_ekle("armut", "armut 2")

    root = Tk()
    gui = MainWindow(root)
    root.mainloop()
