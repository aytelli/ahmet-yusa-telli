# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 10:17:47 2020

@author: ayusa
"""

import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt 
import time

video_name = "kayisi_20_v2.mp4"

threshold = 298
foods = []
ID = 0

# RGB değil BGR
blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0 ,255)
yellow = (0, 255, 255)
rose_dust = (111, 94, 158)
cyan = (255, 153, 51)
black = [0,0,0]

# kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE,(3,3))
fgbg = cv.bgsegm.createBackgroundSubtractorMOG()

foods_colors = []
areas = []


# import plotly.graph_objs as go 
# def visualization_2():
# 	global foods_colors
# 	foods_colors = np.array(foods_colors)
# 	l = len(foods_colors) // 2
# 	blues_c1 = foods_colors[0:l,0]
# 	greens_c1 = foods_colors[0:l,1]
# 	reds_c1 = foods_colors[0:l,2]
# 	
# 	blues_c2 = foods_colors[l:,0]
# 	greens_c2 = foods_colors[l:,1]
# 	reds_c2 = foods_colors[l:,2]
# 	
# 	fig_br = go.Figure()
# 	fig_br.add_trace(go.Scatter(x=blues_c1, y=reds_c1, 
# 							 mode='markers', name='Blue-Red C1', 
# 							 marker_color='rgba(0,0,255,.9)'))
# 	fig_br.add_trace(go.Scatter(x=blues_c2, y=reds_c2, 
# 							 mode='markers', name='Blue-Red C2', 
# 							 marker_color='rgba(255,0,0,.9)'))
# 	fig_br.show()

def visualization():
	global foods_colors
	foods_colors = np.array(foods_colors)
	l = len(foods_colors) // 2
	blues_c1 = foods_colors[0:l,0]
	greens_c1 = foods_colors[0:l,1]
	reds_c1 = foods_colors[0:l,2]
	
	blues_c2 = foods_colors[l:,0]
	greens_c2 = foods_colors[l:,1]
	reds_c2 = foods_colors[l:,2]
	
	fig_bg = plt.figure()
	plt.scatter(blues_c1, greens_c1, c="blue", marker='+',label="Blue-Green C1")
	plt.scatter(blues_c2, greens_c2, c="green", marker='x',label="Blue-Green C2")
	plt.xlabel("Blues")
	plt.ylabel("Greens")
	plt.title("Maviler - Yeşiller")
	plt.legend(loc="upper center", bbox_to_anchor=(0.5, -0.15), ncol= 2)
	plt.show()
	
	fig_br = plt.figure()
	plt.scatter(blues_c1, reds_c1, c="red", marker='v',label="Blue-Red C1")
	plt.scatter(blues_c2, reds_c2, c="blue", marker='^',label="Blue-Red C2")
	plt.xlabel("Blues")
	plt.ylabel("Reds")
	plt.title("Maviler - Kırmızılar")
	plt.legend(loc="lower center", bbox_to_anchor=(0.1, -0.05), ncol= 2)
	plt.show()
	
	fig_gr = plt.figure()
	plt.scatter(greens_c1, reds_c1, c = "red", marker= '<',label="Green-Red C1")
	plt.scatter(greens_c2, reds_c2, c = "orange", marker= '>',label="Green-Red C2")
	plt.xlabel("Greens")
	plt.ylabel("Reds")
	plt.title("Yeşiller - Kırmızılar")
	plt.legend(loc="lower center", bbox_to_anchor=(0.1, -0.05), ncol= 2)
	plt.show()
	
	



class food():
	def __init__(self, x, y, w, h, ID, image):
		self.apricot = []
		self.minx = x
		self.miny = y
		self.maxx = w
		self.maxy = h
		self.ID = ID
		self.center_x = (self.minx + self.maxx) // 2
		self.center_y = (self.miny + self.maxy) // 2
		self.apricot.append([self.minx, self.miny, self.maxx,self.maxy, self.center_x,self.center_y])
		self.size = (self.maxx-self.minx)*(self.maxy-self.miny)
		print("Kayısının boyutu: ",self.size)
		self.blue_mean = 0
		self.green_mean = 0
		self.red_mean = 0
		
		self.extract_pixels(image)
		
	def extract_pixels(self, image):

		image = image[self.miny:self.maxy, self.minx: self.maxx]
		first_temp = first[self.miny:self.maxy, self.minx: self.maxx]
		
# 		delta_frame = cv.absdiff(first_temp, image)
# 		ret, thresh = cv.threshold(delta_frame, 50,255, cv.THRESH_BINARY)
		
		thresh = fgbg.apply(first_temp)
		thresh = fgbg.apply(image)
		
# 		cv.imshow("thres kayisi",thresh)
# 		cv.imshow("kayısının tam resmi:",image)
# 		print("thres 00:",thresh[0,0,1])
		print("thresh shape:",thresh.shape)
		print("thresh 00:",thresh[0,0])
		print("imshape: {}, {}, {}".format(image.shape, image.shape[0], image.shape[1]))
# 		colors = []
		red_pixels = []
		green_pixels = []
		blue_pixels = []
		
# 		if thresh[0,0,0] == 0 and thresh[0,0,1] == 0 and thresh[0,0,2] == 0:
#  			print("00 is black")

		for x in range(image.shape[0]):
			for y in range(image.shape[1]):
				if not thresh[x,y] == 0 : 	# apply varsa
# 				if not (thresh[x,y,0] == 0 and thresh[x,y,1] == 0 and thresh[x,y,2] == 0):	# threshold
# 					foods_colors.append(image[x,y])
					red_pixels.append(image[x,y,2])
					green_pixels.append(image[x,y,1])
					blue_pixels.append(image[x,y,0])
 					
		
		print("Red: :",len(red_pixels))
		print("Grenn:: ",len(green_pixels))
		print("Blues: ", len(red_pixels))
		
		self.blue_mean = sum(blue_pixels) / len(blue_pixels)
		self.green_mean = sum(green_pixels) / len(green_pixels)
		self.red_mean = sum(red_pixels) / len(red_pixels)
		
		print("Blue mean: {}, Green Mean: {}, Red Mean: {}".format(self.blue_mean, self.green_mean, self.red_mean))
		foods_colors.append([self.blue_mean,self.green_mean, self.red_mean])
# 		image[y1:y2, x1:x2]
		
	
	def add(self,x,y, w,h):
		print("Add to apricots:",x,y)
		center_x = (x+w) //2
		center_y = (y+h) //2
		self.apricot.append([x,y,w,h,center_x, center_y])
# 		self.minx = x
# 		self.maxx = w
# 		self.miny = y
# 		self.maxy = h
# 		self.center_x = center_x
# 		self.center_y = center_y
	
	
	def isNear(self, x,y, miny_p): 	# center_x - center_y - minY
# 		d = 10000000
		n = (x,y)
		n = np.array(n)
		found = False
		# bir kayısının x (yatay) boyutu kadar kenarlara daha bakıyoruz.
		img_threshold = self.maxy - self.miny
		
		# bir obje geldikten sonra aynı hizadan başka obje gelebilir sonra
# 		if y < self.center_y:
		if y < self.apricot[-1][5]:
 			return False, -1
		
		for v in self.apricot:
			v = np.array(v)
			minx = v[0]
			maxx = v[2]
			center_x = v[4]
			center_y = v[5]
# 			arr = np.array(center_x, center_y)
# 			p = np.array(v[4], v[5])
# 			dist = np.linalg.norm(arr - n)
# 			if dist < d:
# 				yakin = v
# 				d = dist
# 			print("minx: {} maxx: {} center Y: {}  Gelen CenterX: {} Gelen Y miny: {}".format(minx,maxx, center_y, x, y))
			if miny_p > center_y and x < maxx + img_threshold and x > minx - img_threshold : 		# y-maxy < threshold --- (y > maxy) and 
				found = True
				
		if found:
# 			print("Bu {} bunun {} yakınında.".format(maxy, y))
			return True, self.ID
		return False, -1


start_time = time.time()

cap = cv.VideoCapture(video_name)

_, first = cap.read()
first_frame = cv.cvtColor(first, cv.COLOR_BGR2GRAY)
frame_counter = 1


height = np.size(first_frame,0)
width = np.size(first_frame,1)

original_image = np.ndarray(shape=(height,width,3),dtype=float)


# for i in range(980):
#  	_, empty = cap.read()

while cap.isOpened():
	ex, original_image = cap.read()
	frame_counter += 1
	# negative = 255 - frame
	if not ex:
		break


	frame = cv.cvtColor(original_image, cv.COLOR_BGR2GRAY)
	
	delta_frame = cv.absdiff(first_frame, frame)    # ilk frame ile sonraki gelenler arasındaki farkı bulur.

	_, tresh = cv.threshold(delta_frame, 30,255, cv.THRESH_BINARY)
# 	dilation = np.zeros((5,5), np.uint8)
# 	tresh = cv.dilate(tresh, dilation)


# 	tresh = cv.adaptiveThreshold(delta_frame,255,cv.ADAPTIVE_THRESH_MEAN_C,cv.THRESH_BINARY_INV, 5, 1.8)
	# Otsu's thresholding
# 	ret2,tresh = cv.threshold(delta_frame,255,0,cv.THRESH_BINARY+cv.THRESH_OTSU)


# 	tresh = fgbg.apply(first)
# 	tresh = fgbg.apply(original_image)

# 	cv.imshow("thresh",tresh)

	countour,_ = cv.findContours(tresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
	
	for i in countour:
		if cv.contourArea(i) < 1200:
# 			print("Contour area:",cv.contourArea(i))
			continue
		areas.append(cv.contourArea(i))
		print("Frame Num: ",frame_counter)
		print("Contour area:",cv.contourArea(i))
		x1,y1,w,h = cv.boundingRect(i)

		x2 = x1 + w
		y2 = y1 + h
		center_x = (x1 + x1 + w) // 2
		center_y = (y1 + y1 + h) // 2 
		
		found = False
		
		foo_id = ID

		for f in reversed(foods):
			t, foo_id = f.isNear(center_x,center_y, y1)
			if t:
				f.add(x1, y1, x2, y2)
				found = True
				break
			
		if not found:
			ID += 1
			print("NOT FOUND! Add this:",center_x,center_y)
			foo = food(x1,y1, x2, y2, ID, original_image)	# [y1:y2, x1:x2]
			foods.append(foo)
			foo_id = ID
			

		print("ID: {} minxx: {} maxx: {} miny: {} maxy: {} Center X: {} Center Y: {} "
		.format(foo_id, x1,x2, y1, y2,center_x, center_y))

		print("FOODS:")
		for k in foods:
 			print(k.ID,k.center_x, k.center_y)
		print()


# 		for t in foods:
# 			for r in range(1,len(t.apricot)):
# 				c1 = (t.apricot[r-1][4],t.apricot[r-1][5])
# 				c2 = (t.apricot[r][4],t.apricot[r][5])
# 				cv.line(original_image, c1,c2, (0,255,0), 3)

		# cv.drawContours(original_image, i, -1, (0, 255, 255), 3) 
		cv.circle(original_image, (center_x, center_y), 1, red, 5) # center
		cv.rectangle(original_image, (x1, y1), (x2, y2), yellow, 4)
		
		str_id = "ID: " + str(foo_id)
		cv.putText(original_image, str_id, (center_x-15, center_y-15), 
			 cv.FONT_HERSHEY_SIMPLEX, 0.5, cyan, 2)
		
	cv.putText(original_image, "Frame Num: {}".format(str(frame_counter)), (10, 50), 
			 cv.FONT_HERSHEY_SIMPLEX, 0.5, blue, 2)
	cv.imshow("Image Detection: "+video_name,original_image)
 	# print()
# 	key = cv.waitKey(0)
# 	while key not in [ord('q'), ord('k')]:
# 		key = cv.waitKey(0)
#  	# Quit when 'q' is pressed
# 	if key == ord('q'):
# 		print("End FOODS:")
# 		for row in foods:
#  			print(row.ID,row.minx,row.maxx,row.miny,row.maxy,row.center_x,row.center_y)
# #  			for vval in row.apricot:
# # 				 print("Center X: {} Center Y: {}".format(vval[4], vval[5]))
# 		print("FOOD COLORS")
# 		for col in foods_colors:
#  			print(col)
# 		break
	
# visulazition()
print("Toplam Tespit Edilen:",len(foods))
print("Toplam Frame Sayısı: ",frame_counter)
print("Toplam Çalışma Zamanı:",time.time() - start_time)
cv.destroyAllWindows()
	
	
	
	
	