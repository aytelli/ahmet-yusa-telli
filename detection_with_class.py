# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 10:17:47 2020

@author: ayusa

Frame'de ilk kez görünen nesneleri bir class objesine atıyoruz.
Sonraki framede gelen nesneleri bu objelerden hangisine yakın ona bakıyoruz. Yani hangi objenin sonraki görüntüsü olabilir ona bakıyoruz.
En yakın class objesini bulduktan sonra bu objenin içindeki listeye atıyoruz.

Yani her bir nesne için bir sınıf objesi oluşturuluyor. Aynı nesnenin sonraki görüntüleri aynı objenin içindeki listeye atılıyor.

"""


import cv2 as cv
import numpy as np

video_name = "kayisi_v1.mp4"

threshold = 298
foods = []
ID = 0

# RGB değil BGR
blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0 ,255)
yellow = (0, 255, 255)
rose_dust = (111, 94, 158)
cyan = (255, 153, 51)


class food():
	def __init__(self, x, y, w, h, ID):
		self.apricot = []
		self.minx = x
		self.miny = y
		self.maxx = w
		self.maxy = h
		self.ID = ID
		self.center_x = (self.minx + self.maxx) // 2
		self.center_y = (self.miny + self.maxy) // 2
		self.apricot.append([self.minx, self.miny, self.maxx,self.maxy, self.center_x,self.center_y])
	
	
	def add(self,x,y, w,h):
		print("Add to apricots:",x,y)
		center_x = (x+w) //2
		center_y = (y+h) //2
		self.apricot.append([x,y,w,h,center_x, center_y])
# 		self.minx = x
# 		self.maxx = w
# 		self.miny = y
# 		self.maxy = h
# 		self.center_x = center_x
# 		self.center_y = center_y
	
	
	def isNear(self, x,y, miny_p): 	# center_x - center_y - minY
# 		d = 10000000
		n = (x,y)
		n = np.array(n)
		found = False
		# bir kayısının x (yatay) boyutu kadar kenarlara daha bakıyoruz.
		img_threshold = self.maxy - self.miny
		
		# bir obje geldikten sonra aynı hizadan başka obje gelebilir sonra
# 		if y < self.center_y:
		if y < self.apricot[-1][5]:
 			return False, -1
		
		for v in self.apricot:
			v = np.array(v)
			minx = v[0]
			maxx = v[2]
			center_x = v[4]
			center_y = v[5]
			arr = np.array(center_x, center_y)
# 			p = np.array(v[4], v[5])
# 			dist = np.linalg.norm(arr - n)
# 			if dist < d:
# 				yakin = v
# 				d = dist
# 			print("minx: {} maxx: {} center Y: {}  Gelen CenterX: {} Gelen Y miny: {}".format(minx,maxx, center_y, x, y))
			if miny_p > center_y and x < maxx + img_threshold and x > minx - img_threshold : 		# y-maxy < threshold --- (y > maxy) and 
				found = True
				
		if found:
# 			print("Bu {} bunun {} yakınında.".format(maxy, y))
			return True, self.ID
		return False, -1


cap = cv.VideoCapture(video_name)

_, first_frame = cap.read()
first_frame = cv.cvtColor(first_frame, cv.COLOR_BGR2GRAY)
frame_counter = 1


height = np.size(first_frame,0)
width = np.size(first_frame,1)

# for i in range(35):
# 	_, empty = cap.read()

while cap.isOpened():
	ex, original_image = cap.read()
	frame_counter += 1
	# negative = 255 - frame
	if not ex:
		break
	frame = cv.cvtColor(original_image, cv.COLOR_BGR2GRAY)
	
	delta_frame = cv.absdiff(first_frame, frame)    # ilk frame ile sonraki gelenler arasındaki farkı bulur.

	
	_, tresh = cv.threshold(delta_frame, 20,255, cv.THRESH_BINARY)
	dilation = np.zeros((5,5), np.uint8)
	tresh = cv.dilate(tresh, dilation)


	countour,_ = cv.findContours(tresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
	
	for i in countour:
		if cv.contourArea(i) < 1000:
# 			print("Contour area:",cv.contourArea(i))
			continue
		print("Contour area:",cv.contourArea(i))
		x1,y1,w,h = cv.boundingRect(i)

		x2 = x1 + w
		y2 = y1 + h
		center_x = (x1 + x1 + w) // 2
		center_y = (y1 + y1 + h) // 2 
		
		found = False
		
		foo_id = ID

		for f in reversed(foods):
			t, foo_id = f.isNear(center_x,center_y, y1)
			if t:
				f.add(x1, y1, x2, y2)
				found = True
				break
			
		if not found:
			ID += 1
			print("NOT FOUND! Add this:",center_x,center_y)
			foo = food(x1,y1, x2, y2, ID)
			foods.append(foo)
			foo_id = ID
			

		print("ID: {} minxx: {} maxx: {} miny: {} maxy: {} Center X: {} Center Y: {} "
		.format(foo_id, x1,x2, y1, y2,center_x, center_y))

		print("FOODS:")
		for k in foods:
 			print(k.ID,k.center_x, k.center_y)
		print()


# 		for t in foods:
# 			for r in range(1,len(t.apricot)):
# 				c1 = (t.apricot[r-1][4],t.apricot[r-1][5])
# 				c2 = (t.apricot[r][4],t.apricot[r][5])
# 				cv.line(original_image, c1,c2, (0,255,0), 3)

		# cv.drawContours(original_image, i, -1, (0, 255, 255), 3) 
		cv.circle(original_image, (center_x, center_y), 1, red, 5) # center
		cv.rectangle(original_image, (x1, y1), (x2, y2), yellow, 4)
		
		str_id = "ID: " + str(foo_id)
		cv.putText(original_image, str_id, (center_x-15, center_y-15), 
			 cv.FONT_HERSHEY_SIMPLEX, 0.5, cyan, 2)
		
	cv.putText(original_image, "Frame Num: {}".format(str(frame_counter)), (10, 50), 
			 cv.FONT_HERSHEY_SIMPLEX, 0.5, blue, 2)
	cv.imshow("Image Detection: "+video_name,original_image)
# 	print()
	key = cv.waitKey(0)
	while key not in [ord('q'), ord('k')]:
		key = cv.waitKey(0)
	# Quit when 'q' is pressed
	if key == ord('q'):
		print("End FOODS:")
		for row in foods:
			print(row.ID,row.minx,row.maxx,row.miny,row.maxy,row.center_x,row.center_y)
#  			for vval in row.apricot:
# 				 print("Center X: {} Center Y: {}".format(vval[4], vval[5]))
		
		break
	
print("Toplam Tespit Edilen:",len(foods))
cv.destroyAllWindows()
	
	
	
	
	